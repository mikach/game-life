(function() {

window.APP = function(config) {

	config = config || {};

	this.current = []; // current generation
	this.next = []; // next generation
	this.canvas = config.canvas || document.getElementById('field');
	this.settings = config.form || document.getElementById('settings');

	this.sizeInput = config.sizeInput || document.getElementById('size');
	this.intervalInput = config.intervalInput || document.getElementById('interval');

	this.defaultSize = 20;
	this.defaultInterval = 1;
	this.height = 500;

	this.size = this.interval = undefined;

	return this;

}



APP.prototype.init = function() { // bind form submit

	var app = this;

	if (typeof this.canvas.getContext === "undefined")
		throw "Function getContext is not supported in your browser";

	this.settings.addEventListener('submit', function(ev) {
		ev.preventDefault();
		var size = app.sizeInput.value;
		var interval = app.intervalInput.value;
		app.settings.style.display = 'none';
		app.start(size, interval);
	}, false);

}

APP.prototype.start = function(size, interval) {

	this.size = size || this.defaultSize;
	this.interval = interval || this.defaultInterval;

	for (var i = 0; i < this.size; i++) {
		this.current[i] = [];
		this.next[i] = [];
	}

	this.drawField();
	this.randomize();
	this.generation(this.current, this.next); // draw current generation, calculate next generation

}

APP.prototype.drawField = function() {

	var app = this,
		ctx = app.canvas.getContext('2d'),
		w = app.height / app.size;

	ctx.beginPath();
	for (var i = 0; i <= app.size; i++) {
		ctx.moveTo(0, i*w);
		ctx.lineTo(app.height, i*w);
		ctx.moveTo(i*w, 0);
		ctx.lineTo(i*w, app.height);
	}
	ctx.stroke();

}

APP.prototype.randomize = function() {

	var app = this;

	for (var i = 0; i < app.size; i++) {
		for (var j = 0; j < app.size; j++) {
			var r = Math.random();
			app.current[i][j] = r > 0.7 ? 1 : 0;
		}
	}

}

APP.prototype.generation = function(cur, next) {

	var app = this,
		size = app.size,
		w = app.height / size;


	var ctx = app.canvas.getContext('2d');

	for (var i = 0; i < size; i++) {
		for (var j = 0; j < size; j++) {
			next[i][j] = 0;
			
			// calculation part
			var sum = 0;
			if (i > 0) {
				sum += cur[i-1][j-1] || 0; // if value if undefined return zero
				sum += cur[i-1][j+1] || 0;
				sum += cur[i-1][j] || 0;
			}
			if (i < size-1) {
				sum += cur[i+1][j-1] || 0;
				sum += cur[i+1][j+1] || 0;
				sum += cur[i+1][j] || 0;
			}
			sum += cur[i][j+1] || 0;
			sum += cur[i][j-1] || 0;

			if ((cur[i][j] == 0) && (sum == 3)) next[i][j] = 1;
			if (cur[i][j] == 1) {
				if ((sum >=2) && (sum <= 3)) 
					next[i][j] = 1;
				else
					next[i][j] = 0;
			}

			// clear rectangle and draw new
			ctx.clearRect(i*w+1, j*w+1, w-2, w-2);
			if (cur[i][j] > 0) 
				ctx.fillRect(i*w+1, j*w+1, w-2, w-2);
		}
	}

	// call this function again and swap current and next arrays
	setTimeout(function() {
		app.generation.call(app, next, cur);
	}, app.interval*1000);

}


})();
